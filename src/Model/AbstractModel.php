<?php
/**
 * @author Jörn Heinemann <joernheinemann@gmx.de>
 * @since 2020/04/25
 */

namespace Joern\ApiStart\Model;

use Gram\Mvc\Lib\Factories\DB;
use Gram\Project\Lib\DB\DBInterface;

/**
 * Class AbstractModel
 * @package Joern\ApiStart\Model
 *
 * Ein Model mit allgemeinen Db Functions
 */
abstract class AbstractModel
{
	/** @var DBInterface */
	protected $db;

	/**
	 * AbstractModel constructor.
	 * @param DBInterface $db
	 * Erwartet eine DB Instance per DI für alle weiteren Models
	 */
	public function __construct(DBInterface $db)
	{
		$this->db = $db;
	}

	/**
	 * Gebe für Static aufrufe eine DB Instance zurück
	 *
	 * Kann auch überschrieben werden
	 *
	 * Sollte nur verwendet werden wenn die Models
	 * nicht per DI geladen werden können
	 *
	 * @return DBInterface
	 */
	public static function getDB(): DBInterface
	{
		return DB::db();
	}

	/**
	 * Gibt den Stmt string für IN() zurück
	 *
	 * @param array $array
	 * @return string
	 */
	protected function getIn(array $array)
	{
		return str_repeat("?,", count($array)-1) . "?";
	}

	/**
	 * Insert mehrere Datensätze unbekannter Anzahl auf einmal
	 *
	 * Ein Insert anstatt ganz viele
	 *
	 * Bereite die Placeholder (?) vor mit @see getIn
	 * und lösche die Array Keys aus dem insert data (wegen ? placeholdern)
	 *
	 * Führt ebenfalls auch ein ON DUPLICATE KEY UPDATE mit den Feldern aus
	 *
	 * @param string $table
	 * Der Tabellenanme wo insert werden soll
	 *
	 * @param array $dataField
	 * Die Felder in der Tabelle in die insert werden soll
	 *
	 * @param array $rows
	 * Die eigentlichen Daten als zwei dim. Array
	 * 1. dim => Datensätze
	 * 2. dim => die Daten in dem Datensatz die in die Felder insert werden sollen
	 *
	 * @param bool $onDupKey
	 * Wenn ON DUPLICATE KEY UPDATE auch ausgeführt werden soll
	 *
	 * @return bool
	 */
	protected function multipleInsert(string $table,array $dataField, array $rows, bool $onDupKey = false)
	{
		$placeholders = [];
		$insertData = [];

		//Durchlaufe die unterschiedlichen Datensätzen
		foreach ($rows as $row) {
			$placeholders[] = '('.$this->getIn($row).')';

			//Data darf keine Array Keys mehr haben
			array_push($insertData, ...array_values($row));
		}

		$sql = "INSERT INTO $table (". implode(",", $dataField) .") VALUES ".implode(",",$placeholders);

		if($onDupKey) {
			$updateCols = [];

			foreach ($dataField as $item) {
				$updateCols[] = $item . " = VALUES($item)";
			}

			$sql.= " ON DUPLICATE KEY UPDATE ".implode(', ', $updateCols);
		}

		return $this->db->query($sql,$insertData);
	}

	/**
	 * Fetcht nur eine "Seite" anstatt alle Datensätze
	 *
	 * Bindet limit und offset (die Seite die fetecht werden soll) an die query
	 *
	 * Bsp:
	 * $sql = SELECT * FROM page WHERE id = :id
	 * $limit = 20
	 * start = 1 (page / offset)
	 * $params = [:id=>1] => id = 1
	 *
	 * $this->fetchData($sql,$limit,$start,$params);
	 *
	 * @param string $sql
	 * Die sql Anweisung
	 * Danach wird limit und offset hinten dran gehängt
	 *
	 * @param int $limit
	 * gibt an wie viele Datensätze eine Seite enthalten soll
	 *
	 * @param int $page
	 * die aktuelle Seite die gefetcht werden soll (beginnend bei 1)
	 *
	 * @param array $params
	 * Param müssen hier namen haben
	 * @param null|int $fetchStyle
	 * @return array|bool
	 * gibt false zurück wenn es keine Ergebnisse gab oder die Query fehlgeschlagen ist
	 */
	protected function simplePagination(string $sql, int $limit, int $page, array $params = [], $fetchStyle = null)
	{
		$sql .= " LIMIT :limit OFFSET :offset";

		$stmt = $this->db->prepare($sql);

		$offset = ($page - 1)  * $limit;

		//Int Param binden
		$stmt->bindParam(':limit', $limit, \PDO::PARAM_INT);
		$stmt->bindParam(':offset', $offset, \PDO::PARAM_INT);

		foreach ($params as $param => $var) {
			//binde weitere params [:name] => value
			$stmt->bindParam($param,$var);
		}

		$stmt->execute();

		if($stmt->rowCount() <=0) {
			return false;
		}

		return $stmt->fetchAll($fetchStyle);
	}
}