<?php
/**
 * @author Jörn Heinemann <joernheinemann@gmx.de>
 * @since 2020/04/30
 */

namespace Joern\ApiStart\Middleware;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

abstract class AbstractMiddleware implements MiddlewareInterface
{
	/** @var RequestHandlerInterface */
	protected $fallback;

	public function __construct(RequestHandlerInterface $fallback)
	{
		$this->fallback = $fallback;
	}

	protected function fallback(ServerRequestInterface $request, $handler)
	{
		return $this->fallback->handle($request->withAttribute('callable',$handler));
	}

	protected function getUserInfo(ServerRequestInterface $request,string $param)
	{
		return self::getUser($request)[$param] ?? false;
	}

	/**
	 * Gebe die Parameter der Route für den Request zurück
	 *
	 * @param ServerRequestInterface $request
	 * @param string $name
	 * @param null $default
	 * @return null
	 */
	protected function getParam(ServerRequestInterface $request, string $name, $default = null)
	{
		$param = $request->getAttribute("param",[]);

		return $param[$name] ?? $default;
	}

	public static function getUser(ServerRequestInterface $request)
	{
		return $request->getAttribute("user",false);
	}
}