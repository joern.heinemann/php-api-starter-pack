<?php
/**
 * @author Jörn Heinemann <joernheinemann@gmx.de>
 * @since 2020/04/30
 */

namespace Joern\ApiStart\Middleware;

use Firebase\JWT\JWT;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Class JWTAuthentication
 * @package Joern\ApiStart\Middleware
 *
 * Hole und prüft das Jwt
 *
 * Bereite zudem die User Info aus dem Jwt vor
 * und packe diese in den request
 */
class JWTAuthentication implements MiddlewareInterface
{
	/** @var array */
	protected $options = [
		"algorithm" => ["HS256", "HS512", "HS384"],
		"regexp" => "/Bearer\s+(.*)$/i",
		"cookie" => "jwt"
	];

	/** @var string */
	protected $jwt_key;

	/**
	 * Authentication constructor.
	 *
	 * @param string $key
	 * Der Jwt Secret Key
	 */
	public function __construct(string $key)
	{
		$this->jwt_key = $key;
	}

	/**
	 * @inheritdoc
	 */
	public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
	{
		if(false === $jwt = $this->getFromRequest($request)) {
			return $handler->handle($request->withAttribute("user",false));
		}

		if(false === $decoded = $this->decode($jwt)) {
			return $handler->handle($request->withAttribute("user",false));
		}

		$userObject = $decoded->data ?? null;

		if(isset($userObject)) {
			$user = [
				"id"=>$userObject->id ?? null,
				"username"=>$userObject->email ?? null
			];
		} else {
			$user = false;
		}

		return $handler->handle($request->withAttribute("user",$user));
	}

	/**
	 * Decodire das Jwt mit @see JWT
	 *
	 * @param string $jwt
	 * @return bool|\object
	 * Gebe false zurück bei Fehlern
	 * Gebe das Jwt als std class object zurück
	 */
	protected function decode(string $jwt)
	{
		try {
			return JWT::decode(
				$jwt,
				$this->jwt_key,
				(array) $this->options["algorithm"]
			);
		} catch (\Exception $exception) {
			return false;
		}
	}

	/**
	 * Hole das Token vom Request
	 *
	 * Kann entweder im Authorization Header als Bearer sein (OAuth 2.0)
	 *
	 * Oder im Cookie, dort ebenfalls als Bearer oder einfach unter dem jwt cookie
	 *
	 * @param ServerRequestInterface $request
	 * @return bool
	 */
	protected function getFromRequest(ServerRequestInterface $request)
	{
		$header = $request->getHeaderLine("Authorization");

		if(!empty($header)) {
			//Token war im Header
			if (preg_match($this->options["regexp"], $header, $matches)) {
				return $matches[1];
			}
		}

		$cookieParams = $request->getCookieParams();

		if(isset($cookieParams[$this->options["cookie"]])) {
			//War im Cookie
			if(preg_match($this->options["regexp"], $cookieParams[$this->options["cookie"]], $matches)) {
				return $matches[1];
			}

			return $cookieParams[$this->options["cookie"]];
		};

		return false;
	}
}