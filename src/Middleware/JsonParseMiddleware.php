<?php
/**
 * @author Jörn Heinemann <joernheinemann@gmx.de>
 * @since 2020/04/23
 */

namespace Joern\ApiStart\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Class JsonParseMiddleware
 * @package Joern\ApiStart\Middleware
 *
 * Wandelt den Input string zu json um und
 * packt diesen in den parsed body vom Request
 */
class JsonParseMiddleware implements MiddlewareInterface
{
	/** @var bool */
	private $assoc;

	/** @var int */
	private $depth;

	/** @var int */
	private $options;

	public function __construct(bool $assoc = true, int $depth = 512, int $options = 0)
	{
		$this->assoc = $assoc;
		$this->depth = $depth;
		$this->options = $options;
	}

	/**
	 * @inheritdoc
	 */
	public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
	{
		$request = $request->withParsedBody($this->parse($request->getBody()));

		return $handler->handle($request);
	}

	private function parse(StreamInterface $stream)
	{
		$json = trim((string) $stream);

		$data = json_decode($json, $this->assoc, $this->depth, $this->options);

		if ($this->assoc) {
			return $data ?: [];
		}

		return $data;
	}
}